package com.ncamc.common.enums;

/**
 * 操作人类别
 *
 * @author hugaoqiang
 */
public enum OperatorType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
